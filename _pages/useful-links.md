---
title: Useful Links
date: 2019-10-09
classes: wide
permalink: /useful-links/
---

We believe that the first step in addressing bias in academia is
becoming educated about the issues. Here, we link to several other pages
that have proven insightful in developing our own opinions about the
issues. Many of these pages offer different points of view and focus on
academic fields other than linguistics, placing this work in a broader
perspective.

<ol>
{%- for useful_link in site.data.useful-links -%}
<li>
  <a href="{{  useful_link.url  }}">{{  useful_link.name  }}</a>
</li>
{%- endfor -%}
</ol>
