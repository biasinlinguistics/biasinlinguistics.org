---
title: Projects
date: 2019-10-09
classes: wide
permalink: /projects/
---

The three topics we have investigated in depth are:

- [Career Stages][career-stages]

- [Conference Invitees][conference-invitees]

- [Publication Rates][publication-rates]

[career-stages]: /projects/career-stages/
[conference-invitees]: /projects/conference-invitees/
[publication-rates]: /projects/publication-rates/
