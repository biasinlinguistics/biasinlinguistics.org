---
title: Bias in Linguistics
classes: wide
date: 2019-10-09
permalink: /
---

Gender bias is pervasive, and studies have shown repeatedly that there
is a leaky pipeline in academia: at each successive stage of the
academic hierarchy, the proportion of women decreases. Our work shows
that this is true in linguistics as well. This is particularly true
after graduate school: while more than 50% of graduate students in
linguistics are women, only about 35% of assistant professors are
women. Of academics who have received tenure, about 55% of associate
professors are women but only about 35% of full professors are women.

{:refdef: style="text-align: center;"}
![Graph of proportion of women at each career stage](/assets/img/career_stages.jpeg)
{: refdef}

These numbers may reflect a great number of influences. Our ongoing work
aims to continue data collection to allow for longitudinal analyses and
digs in more precisely to specific causes and symptoms of imbalanced
representation in the field.

The results we present here are only a first step towards a data-based
approach to understanding bias in linguistics. Ultimately, we would like
to use such results to inform meaningful solutions for the issue.
