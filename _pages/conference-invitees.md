---
title: Conference Invitees
date: 2019-10-09
toc: true
toc_label: Conference Invitees
permalink: /projects/conference-invitees/
---

## Motivation and Takeaways

Invited speakers at conferences tend to be those who have received the
most recognition for their work in their subfield. By collecting data on
the representation of women across invited talks, we can ask whether it
seems that women faculty members are being recognized at the same rate
as men. Considering the field as a whole, there does not seem to be a
discrepancy between the gender proportions of faculty and invited
speakers, according to this dataset. However, contrary to anecdotal
evidence, in this dataset women invited speakers are under-represented
in Phonology/Phonetics and Sociolinguistics conferences. Furthermore,
women conference invitees in Experimental fields are over-represented
compared to the actual proportion of women faculty members in those
fields.

## Data

<iframe src="https://shiny.adamliter.org/genderbiasinlinguistics" width="100%" height="1000"></iframe>

## Methodology

[LingAlert][LingAlert] was used to make a list of recent conferences and
their invited speakers. Specific details about the conferences were
manually collected from each conference website. For each conference,
the following was recorded: year of the conference, region, main topic,
theoretical bias, and language bias. Each invited speaker’s CV or
website was used to determine their graduating university, current
affiliation, perceived gender, and year of graduation. The subfields
represented in this data are the same as in our Career Stages data.

- Number of Conferences: World = 48; North America = 22
- 87% of the conference invitee data is after 2003.

The data above was updated in November 2016. More recently collected
data will be added in the coming months.

## Data Download

You can download the data using the "Raw Data" tab in the display above.


<!-- links -->
[LingAlert]: https://lingalert.com/
