---
title: About
date: 2019-10-09
classes: wide
permalink: /about/
---

We are interested in looking at various types of bias in the academic
field of linguistics. Obviously, many types of bias are difficult to
quantify. This website specifically looks at gender proportions at
various stages of the academic career, from undergraduates through
faculty. We have also collected data on conference invitees and
publication rates, the idea being that both of these are ways in which
academics receive recognition for their work in their subfield. At the
moment, our project specifically looks at gender bias, but we are
actively searching for ways to pursue research on bias based on other
factors such as race, native language, or sexual orientation. We
encourage everyone to be aware of the biases that exist in academia and
make positive changes where they see an opportunity.

Our goal with this webpage is to raise awareness of the issue, provide
our analyses and some discussion, and to make available data that we
collect so that anyone can explore the data on their own. Check out our
various projects using the menu above, or take a look at our [current
and future directions][future-directions]. We also have a [collection of
several other resources][useful-links] that have been helpful in shaping
our thoughts about these issues.

Additionally, we'd like to encourage those who are interested in the
project to make suggestions on how to improve the website, data
collection, awareness raising, or any other aspect of the
project. Please feel free to check out our collaborators or contact us
using the information below:

{% assign collab_array = site.empty_array %}
{% assign collab_array = collab_array | push: site.data.authors["Bethany Dickerson"] %}
{% assign collab_array = collab_array | push: site.data.authors["Karthik Durvasula"] %}
{% assign collab_array = collab_array | push: site.data.authors["Adam Liter"] %}

<ul>
{%- for collab in collab_array -%}
  {% assign wbs = collab.links | where_exp:"item", "item.label == 'Website'" %}
  {% assign website = wbs[0] %}
  {% assign eml = collab.links | where_exp:"item", "item.label == 'Email'" %}
  {% assign email = eml[0] %}

  <li>
  {%- if website.url -%}
    <a href="{{ website.url }}">{{ collab.name }}</a>
  {%- else -%}
    {{  collab.name  }}
  {%- endif -%}
  {%- if email.url -%}
    <a href="{{ email.url }}">
      <i class="{{ email.icon | default: 'fas fa-link' }}" aria-hidden="true">
      </i>
    </a>
  {%- endif -%}
  </li>
{%- endfor -%}
</ul>

<!-- links -->
[future-directions]: /future-directions/
[useful-links]: /useful-links/
