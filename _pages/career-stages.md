---
title: Career Stages
date: 2019-10-09
toc: true
toc_label: Career Stages
permalink: /projects/career-stages/
---

## Motivation and Takeaways

By collecting data on the proportion of women in various academic
positions in the field of linguistics, we can see that the field of
linguistics, like many other STEM fields, has a leaky pipeline
problem. In general, as one goes up the traditional academic hierarchy
(from undergraduate student to graduate student to faculty member),
there is a decrease in the representation of women.

## Data

<iframe src="https://shiny.adamliter.org/genderbiasinlinguistics" width="100%" height="1000"></iframe>

## Methodology

### Faculty data

We collected faculty data from 50 universities in the United States with
linguistics departments/programs. Faculty data was collected by going to
individual institution websites and using people’s websites or CVs to
annotate for perceived gender and subfield.

Subfields were determined using the research interest or personal
biographies section of institutions' "People" page. These were checked
against individuals' CVs, including lists of publications. Each
individual person is coded for every subfield they do research in. This
is to ensure that we are making relevant comparisons: a person who
specializes in both acquisition and syntax, for example, is presumably
going to conferences and advising students in *both* of these fields.

#### What is included in each group


- Applied: second-language or pedagogy related
- Experimental: Child Language Acquisition and Psycholinguistics
- Phonology & Phonetics: Phonology and Phonetics
- Sociolinguistics: Sociolinguistics
- Syntax & Semantics: Syntax, Semantics, Morphology


### Student data

Student data reflect enrollment in the 2016-2017 academic year.

An email survey was sent to 50 institutions with linguistics programs in
the United States. This survey asked department secretaries or advisors
to provide us with a count of how many male and female graduate students
are currently concentrating in certain areas. We also asked about the
number of male and female undergraduates majoring in linguistics.

As we discussed in the [disclaimers][disclaimers] section, we are
currently focused on differences in outcomes based on gender, not
sex. However, at the time this survey was conducted, we were not careful
about distinguishing between the two and so the survey itself referred
to "male" and "female" students. We do not know if people responded to
the question exactly as it was phrased or if they responded as if we
meant gender, and we are unsure of the extent to which this error
impacts our findings.

The survey is reproduced below:

- How many female/male undergraduate students are majoring in
  linguistics at your institution? [example answer: 20 female / 20 male]
- How many female/male graduate students are specializing in Semantics,
  Syntax, and/or Morphology?
- How many female/male graduate students are specializing in Phonology
  and/or Phonetics?
- How many female/male graduate students are specializing in
  Sociolinguistics?
- How many female/male graduate students are specializing in Language
  Acquisition?
- How many female/male graduate students are specializing in a subfield
  other than those listed above (psycho/neurolinguistics, applied
  linguistics, speech pathology, *etc.*) or in more than one of the
  above subfields?

#### NSF and NCES data

In addition, we gathered data from NSF about the number of Linguistics
PhDs earned in the US per year by males and females since 1958. Data
from the NCES provides information on the proportion of females earning
BAs, MAs, and PhDs in linguistics in the US since the early
’90s. (Please note the different time periods available.)

Again, we are interested in gender, not sex. However, the NSF and NCES
report data on sex differences. We are unable to estimate the extent to
which this discrepancy impacts our conclusions.

### LSA Executive Committee

Data on the makeup of the Executive Committee was gathered from the [LSA
website][lsa] for each year since 2005.

## Data Download

You can download the data using the "Raw Data" tab in the display above.

<!-- links -->
[disclaimers]: /disclaimers/
[lsa]: https://www.linguisticsociety.org/content/past_executive_committees
