---
title: Collaborators
date: 2019-10-09
classes: wide
permalink: /collaborators/
---

## Research conducted by

<ul>
{%- for a in site.data.authors -%}
  {% assign p = a[1] %}
  {% assign wbs = p.links | where_exp:"item", "item.label == 'Website'" %}
  {% assign website = wbs[0] %}
  {% assign eml = p.links | where_exp:"item", "item.label == 'Email'" %}
  {% assign email = eml[0] %}
  <li>
    {%- if website.url -%}
      <a href="{{ website.url }}">{{ p.name }}</a>
    {%- else -%}
      {{  p.name  }}
    {%- endif -%}
    {%- if email.url -%}
      <a href="{{ email.url }}">
        <i class="{{ email.icon | default: 'fas fa-link' }}" aria-hidden="true">
        </i>
      </a>
    {%- endif -%}
  </li>
{%- endfor -%}
</ul>

## Thanks

We would like to thank the following people and groups for their help
and discussions:

- [Virginia Valian][virginia] for invaluable discussion and guidance.
- [Alyson Reed, David Robinson, and Brian Joseph from LSA][lsa] for
  helping us to locate information.
- [Mark K. Fiegener from NSF][mark] for providing us with information on
  PhD recipients.
- [Kai von Fintel][kai] for discussion about terminology.
- [Andries W. Coetzee][andries] for providing us with contacts in the
  LSA.
- [Donca Steriade][donca] and [Joe Pater][joe] for helpful suggestions
  about the function of the website.
- [UMD's LSC][lsc], especially the [Winter Storm][ws2018] (and [Michelle
  Erskine][michelle]) and [Policy Committee][lsc-policy] (and [Lara
  Ehrenhofer][lara]) for providing us with several platforms to
  publicize our results and recruit volunteers.
- Audiences at the 2019 LSA Annual Meeting.
- All the institutions that responded to our survey.
- And a special thanks to all of the graduate and faculty volunteers who
  have helped move this project forward!

We'd like to thank [Savithry Namboodiripad][savithry], [Corrine
Occhino][corrine], [Lynn Hou][lynn], and [Anne Charity Hudley][anne] for
taking the time to develop a survey on the climate of the field.

We would also like to acknowledge [Kristen Syrett][kristen] and the
[Committee on the Status of Women in Linguistics (COSWL)][coswl]. We
encourage all those interested in such matters to participate in their
independent data collection efforts. Check out their [Pop-up
Mentoring][popup] initiative!

<!-- links -->
[virginia]: https://www.virginiavalian.org/
[lsa]: https://www.linguisticsociety.org/
[mark]: https://www.nsf.gov/staff/staff_bio.jsp?lan=mfiegene&org=NSF&from_org=
[kai]: https://www.kaivonfintel.org/
[andries]: https://sites.lsa.umich.edu/coetzee/
[donca]: http://linguistics.mit.edu/user/steriade/
[joe]: https://blogs.umass.edu/pater/
[lsc]: https://languagescience.umd.edu/
[ws2018]: https://languagescience.umd.edu/ws2018
[michelle]: https://hesp.umd.edu/gradprofile/erskine/michelle
[lsc-policy]: https://languagescience.umd.edu/beyond-umd/policy-and-advocacy
[lara]: https://de.linkedin.com/in/lara-ehrenhofer/de-de
[savithry]: http://idiom.ucsd.edu/~snambood/
[corrine]: https://sites.google.com/view/corrineocchino/home?authuser=0
[lynn]: https://sites.google.com/view/linasigns/about
[anne]: https://annecharityhudley.com/
[kristen]: https://sites.rutgers.edu/kristen-syrett/
[coswl]: https://www.linguisticsociety.org/about/who-we-are/committees/status-women-linguistics
[popup]: https://womeninlinguistics.com/
