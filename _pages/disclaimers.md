---
title: Disclaimers
date: 2019-10-09
classes: wide
permalink: /disclaimers/
---
## Terminology disclaimers

We would like to be clear about the important role of terminology in
this work, specifically the distinction between sex, gender identity and
gender expression, and the non-binary nature of each of these. In
discussing our data we refer to the differences we observe as gender
differences. This is in contrast with sex differences, which we do not
believe our data speak to. That is, the observed differences in outcomes
of men and women in linguistics are, under our assumptions, due to
social factors and not biological factors. To be specific, given our
data collection practices, we interpret the gender expression of
individuals based on their photos and first names, and we believe that
this perceived gender correlates (albeit imperfectly) with gender
identity. This means we are unable to comment on the experiences of
trans and non-binary individuals. Furthermore, we believe that the
interpretation of these modes of gender expression is responsible for
others’ inferences about sex and gender and it is these inferences that
motivate the different treatment that men and women experience. We hope
for future research to explore these questions more adequately.

We refer the reader to [this link][genderbread] for further information
regarding the difference between gender identity and sex.

## Interpretation disclaimers

We believe that bias, recognized or not, manifests in specific actions
which result in patterns of discrimination. The observations we make on
this website are based on our own data, and care should be taken in
extrapolating the results to the whole field.

We do not quantify the actions which cause the pattern we see of lower
proportions of women in each stage of the academic career, though we
hope that our investigation of publication rates is a move toward
this. Given other research on bias in academia, however, we can
speculate on what causes these imbalances. For example, women in
academia are disproportionately burdened with professional service
duties ([Guarino and Borden, 2017][guarino-borden-2017) and
housework/childcare ([Mason, Goulden, and Wolfinger,
2006][mason-etal-2006]); don't benefit from male privilege in abstract
reviews ([Roberts & Verhoef, 2016][roberts-verhoef-2016]); receive
poorer student evaluations ([Mengel, Sauermann, and Zolitz,
2019][mengel-etal-2019]); are written poorer letters of recommendation
([Trix & Pskena, 2003][trix-pskena-2003]], [Madera, Hebl and Martin,
2009][madera-etal-2009]); and experience outright misogyny such as the
sexual harassment and assault. All of these are manifestations of
implicit gender biases and potentially contribute to the attrition rates
that we see, as well as any differences we see in publication
rates. Because we are not directly measuring the above actions, only the
symptom, we urge the readers to be careful in interpreting our
results. However, our data is useful for providing a before/after metric
to determine if interventions have the desired effect.


<!-- links -->
[genderbread]: https://www.itspronouncedmetrosexual.com/wp-content/uploads/2012/01/Genderbread-Person.jpg
[gaurino-borden-2017]: https://doi.org/10.1007/s11162-017-9454-2
[mason-etal-2006]: https://books.google.com/books?hl=en&lr=&id=aFU8KAU8uHkC&oi=fnd&pg=PR7&dq=The+balancing+act:+Gendered+perspectives+in+faculty+roles+and+work+lives&ots=0de6jj-bBY&sig=NWU6okyRm-VpFweK5HZPoHrm3Jc#v=onepage&q&f=false
[roberts-verhoef-2016]: https://doi.org/10.1093/jole/lzw009
[mengel-etal-2019]: https://doi.org/10.1093/jeea/jvx057
[trix-pskena-2003]: https://doi.org/10.1177/0957926503014002277
[madera-etal-2009]: https://doi.org/10.1037/a0016539
