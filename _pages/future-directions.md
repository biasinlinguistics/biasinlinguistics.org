---
title: Future Directions
date: 2019-10-09
classes: wide
permalink: /future-directions/
---

The results we present here are only a first step towards a data-based
approach to understanding bias in Linguistics. Ultimately, we would like
to use such results to come up with meaningful solutions for the issue.

We have been busy growing the network of people working on this
project. In the early stages of the project, the work was presented at
Michigan State University's [Undergraduate Research and Arts Forum
2017][uuraf2017] and at the [Michigan State Undergraduate Linguistics
Conference 2017][msulc2017].

In the past several years, we presented and lead workshops at UMD's
[Winter Storm 2018][ws2018] and formed a working group made up of
graduate students and faculty involved in UMD's [Language Science
Center][lsc]. We have presented at the LSC's [lunch talk series][lslt],
and are working to connect with other like-minded groups addressing
gender and other biases in the field of linguistics. Much of this work
culminated in a presentation at the [LSA 2019 Annual Meeting][lsa2019]
in the Sociology of Linguistics session.

Broadly, these are the goals of this project:

1. Conduct further descriptive work to identify *where* and *why* the bias
   exists, including looking at things like race, native language,
   sexual orientation, *etc.*.
2. Collectively come up with solutions to such problems of bias. We are
   particularly interested in working with departments and institutions
   to think about the policy implications of our data.

Here are some specific goals we are currently addressing:

- Creating a survey for graduate students to investigate how factors
known to affect advancement at the faculty level—such as gender
differences in the amount and type of mentorship, teaching, and service
experiences—may arise even earlier, before students enter the job
market. -- *Paulina Lyskawa and Laurel Perkins*

- Expanding, visualizing, and further analyzing the publication dataset,
  including pursuing submission data. -- *Hanna Muller and Phoebe
  Gaston*

- Designing, building, and maintaining technological infrastructure for
  data storage, analysis, and presentation. This includes the Bias in
  Linguistics website and repositories of code for data analysis. --
  *Adam Liter, Jackie Nelligan, Karthik Durvasula*

- Updating and expanding our current dataset of conference invitees. --
  *Bethany Dickerson, Maggie Kandel, Karthik Durvasula, Max Papillon*

- Updating and expanding our current dataset of graduate students. --
  *Bethany Dickerson and group*

We invite you to help contribute to these efforts. If you'd like to know
more or get involved, please __*contact*__ us or __*sign up for our
listserv*__!


<!-- links -->
[uuraf2017]: https://urca.msu.edu/forums/uuraf-2017
[msulc2017]: https://sites.google.com/site/2017msulc/
[ws2018]: https://languagescience.umd.edu/ws2018
[lsc]: https://languagescience.umd.edu/
[lslt]: https://languagescience.umd.edu/events/lang-sci-lunch-talks-policy-double-header
[lsa2019]: https://www.linguisticsociety.org/event/lsa-2019-annual-meeting
