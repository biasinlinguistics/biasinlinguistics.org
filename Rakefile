COMPILED_DIR = "biasinlinguistics.org"
DEST_DIR = "_site"

task :default => [:build]

desc "Build Jekyll site and copy files to COMPILED_DIR"
task :build do
  Rake::Task["helper:wipe_dest_dir"].invoke
  sh "jekyll build"
  Rake::Task["helper:wipe_compiled_dir"].invoke
  sh "cp -RL _site/* #{COMPILED_DIR}/"
end

desc "Serve Jekyll site locally for development and watch"
task :watch do
  Rake::Task["helper:wipe_dest_dir"].invoke
  Rake::Task["helper:kill_running_jekyll"].invoke
  sh "jekyll serve -w"
end

desc "Serve Jekyll site locally for development purposes"
task :serve do
  Rake::Task["helper:wipe_dest_dir"].invoke
  Rake::Task["helper:kill_running_jekyll"].invoke
  sh "jekyll serve -B"
end

namespace :deploy do

  desc "Check HTML output with htmlproofer"
  task :proof do
    sh "htmlproofer #{COMPILED_DIR} --only-4xx --check-html --allow-hash-href --check-img-http"
  end

  desc "Deploy to production"
  task :deploy_prod do
    sh "rsync -avzHP --exclude='.git biasinlinguistics.org' --delete #{COMPILED_DIR}/ -e ssh adamliter@69.164.209.225:/var/www/biasinlinguistics.org"
  end
end

# Helper rake tasks
namespace :helper do

  desc "Clean DEST_DIR"
  task :wipe_dest_dir do
    sh "rm -rf #{DEST_DIR}/" unless Dir["#{DEST_DIR}/*"].empty?
  end

  desc "Clean COMPILED_DIR"
  task :wipe_compiled_dir do
    sh "rm -r #{COMPILED_DIR}/*" unless Dir["#{COMPILED_DIR}/*"].empty?
  end

  desc "Kill the Jekyll server running in the background"
  task :unserve do
    sh "kill -9 $(pgrep jekyll)"
  end

  desc "Kill Jekyll if already running"
  task :kill_running_jekyll do
    unless `pgrep jekyll`.empty?
      puts("Jekyll is already running. Killing already running process...")
      Rake::Task["helper:unserve"].invoke
    end
  end
end
